# tezos-uri

This package provides a JavaScript implementation of the Tezos URI [standard](https://gitlab.com/tzip/tzip/blob/master/Proposals/TZIP-0008/TZIP-0008.md). It is written in [ReasonML](https://reasonml.github.io/) and compiled to JS.

Currently, when users try to interact with a DApp, they have to copy-paste transaction parameters into their wallet by hand. This interaction is far from frictionless and we've built this package to fix that.

The HTML documentation is [here](https://smartcontractlabs.gitlab.io/tezos-uri/TezosUri/index.html). A step-by-step guide is [here](https://gitlab.com/smartcontractlabs/tezos-uri/blob/master/HOWTO.md).

## Quick Start

### Use with JavaScript with NPM

First install the package from NPM by running the following command:

`npm install tezos-uri`

Then, you can import it into your project:

ES-style import with a bundler (e.g. webpack)

```js
import { encodeMainnet, decodeMainnet } from "tezos-uri";
```
Please note that we don't actually distribute an ES module and this works thanks to the bundler processing your project before deployment. It does not work directly in the browser.

NodeJS require

```js
const tezosUri = require("tezos-uri");
```

### Use with JavaScript without NPM

Simply embed a script tag in your HTML:

```html
<script src="https://unpkg.com/tezos-uri@2.0.1/bin/tezosUri.js" integrity="sha384-jSmeZaZ9MPGFKDxjkpJNppKJuWY0yQL/n/mftVXZt+QJp1JAS6E6f39ZNzSxX0Cz" crossorigin="anonymous"></script>
```

### Use with ReasonML

First install the package from NPM by running the following command:

`npm install tezos-uri`

Then add it to your dependencies in `bsconfig.json`:

```
{
  ...
  "bs-dependencies": ["tezos-uri"]
}
```

The project will be available under the `TezosUri` namespace.

## Reading the code

The code is written in ReasonML, a dialect of OCaml, and then compiled to JavaScript. It's easier to read in an editor that supports the language , such as VSCode (using the [reason-vscode extension](https://marketplace.visualstudio.com/items?itemName=jaredly.reason-vscode)).

You can read the JS files after you build the project, or you can just use the [tezosUri-js branch](https://gitlab.com/smartcontractlabs/tezos-uri/tree/tezosUri-js)

## JavaScript API Reference

### Encode API

Validates and encodes a JSON payment request and returns a Tezos URI. Raises an exception on invalid input.

There are some examples of the JSON object in the section below. The exact specification for the payment request JSON is [here](https://gitlab.com/tzip/tzip/blob/master/Proposals/TZIP-0008/TZIP-0008.md#specification).

```
/**
 * @description Validates and encodes a JSON payment request and returns
 *   a Tezos Mainnet URI. Raises an exception on invalid input.
 * @param {Array<Request>} input JSON payment request
 * @returns {string} Tezos Mainnet URI
 */
export declare function encodeMainnet(input: Array<Request>): string;

/**
 * @description Validates and encodes a JSON payment request and returns
 *   a Tezos Testnet URI. Raises an exception on invalid input.
 * @param {Array<Request>} input JSON payment request
 * @returns {string} Tezos Testnet URI
 */
export declare function encodeTestnet(input: Array<Request>): string;
```

## Decode API

Decodes and validates a Tezos URI and returns the resulting JSON payment request. Raises an exception when the request is invalid, or when the URI does not match the expected network.

```
/**
 * @description Decodes and validates a Tezos URI and returns a resulting
 *   payment request JSON. Raises an exception when the request is invalid,
 *   or when the URI is not a Tezos Mainnet URI.
 * @param {string} uri Tezos Mainnet URI
 * @returns {Array<Request>} payment request JSON
 */
export declare function decodeMainnet(uri: string): Array<Request>;

/**
 * @description Decodes and validates a Tezos URI and returns a resulting
 *   payment request JSON. Raises an exception when the request is invalid,
 *   or when the URI is not a Tezos Testnet URI.
 * @param {string} uri Tezos Testnet URI
 * @returns {Array<Request>} payment request JSON
 */
export declare function decodeTestnet(uri: string): Array<Request>;
```

## JavaScript Examples

### Encode

```js
import { encodeMainnet } from "tezos-uri";

const operation = {
  kind: "transaction",
  amount: "100000",
  destination: "tz1NAP47ubff4JDLJ94UcwEs66dDDAJGRDwN"
};

const link = encodeMainnet([{ content: operation }]);
```

### Decode

```js
import { decodeMainnet } from "tezos-uri";

const decoded = decodeMainnet(link);
```

## ReasonML API Reference

### Encode API

Validates and encodes a JSON payment request and returns a Tezos URI. Raises an exception on invalid input.

There are some examples of the JSON object in the section below. The exact specification for the payment request JSON is [here](https://gitlab.com/tzip/tzip/blob/master/Proposals/TZIP-0008/TZIP-0008.md#specification).

```
let mainnet: Js.Json.t => string
let testnet: Js.Json.t => string
```

See the [Encode.re](https://gitlab.com/smartcontractlabs/tezos-uri/blob/master/src/Encode.re) file for more details.

## Decode API

Decodes and validates a Tezos URI and returns the resulting JSON payment request. Raises an exception when the request is invalid, or when the URI does not match the expected network.

```
let mainnet: string => Js.Json.t
let testnet: string => Js.Json.t
```

See the [Decode.re](https://gitlab.com/smartcontractlabs/tezos-uri/blob/master/src/Decode.re) file for more details.

## ReasonML Examples

### Encode

```reasonml
let operation = {|
  [
    {
      "content": {
        "kind": "transaction",
        "amount": "399000",
        "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg"
      }
    },
    {
      "content": {
        "kind": "transaction",
        "amount": "129000",
        "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg"
      }
    }
  ]
|}

let encodedLink = TezosUri.Encode.testnet(Js.Json.parseExn(operation))
```

### Decode

```reasonml
let decodedObject = TezosUri.Decode.testnet(encodedLink)
```

## Contribute

### How to build

Get the code and the dependencies:

```
git clone https://gitlab.com/smartcontractlabs/tezos-uri.git
cd tezos-uri
npm install
```

Then, to build once, run:

```
npm run build
```

To compile when changes are made, run:

```
npm run start
```

To make a production ready bundle, run:

```
npm run make:build
```

### Testing

The codebase is covered by tests, using the [@glennsl/bs-jest](https://github.com/glennsl/bs-jest) bindings for the [Jest](https://jestjs.io/) testing framework. The tests are run with JavaScript files generated by the [BuckleScript](https://bucklescript.github.io/) compiler (not the ReasonML source files).

You can run these tests with npm:

```
npm run test
```

### Generating Docs

First, set up the documentation generator and generate stylesheets:

```
npm run bsdoc:init
```

Then build the HTML:

```
npm run bsdoc:build
```

## License

MIT

## Credits

This project is built by [Smart Contract Labs](https://smartcontractlabs.ee) and funded by [TQ Tezos](https://tqtezos.com/).