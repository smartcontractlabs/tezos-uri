#!/bin/bash

DIR="public"
if [ -d "$DIR" ]
then
  echo "Copying docs into public folder."
  cp -r docs/* public
  echo "Done."
else 
  echo "Creating public folder"
  mkdir public
  echo "Copying docs into public folder."
  cp -r docs/* public
  echo "Done."
fi