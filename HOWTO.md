# How to use the tezos-uri npm package

## Handling a Tezos operation request
This guide is mainly meant for JavaScript web wallets, but might be useful even if your wallet is using another language or platform.

Using the `tezos-uri` package, you can make it easier for your users to interact with Tezos DApps. Where they now have to manually copy-and-paste transaction parameters into the sending form, they will just click on a payment request link and have the parameters prefilled by your wallet.

### Registering a web protocol handler
To have your wallet open the Tezos links, you first need to register a protocol handler. This is done using the `registerProtocolHanlder` Web API, for example:
```
navigator.registerProtocolHandler("web+tezos",
                                  "https://wallet.example.com/#uri=%s",
                                  "MyWallet Tezos Payment Request Handler");
```
This is currently supported by Firefox, Chrome and Chromium-based browsers.
When your user has confirmed the protocol registration, they can click a web+tezos link, e.g.:
```
web+tezos:4thVcmfFUC7bLTkuHjMZ9PvdcZgUx9AZ9b6hDa1TbafzNjE8sQ8KMuKEqfjqkVv8pP8fX29d9252TUJXCLdUXSFo1nf2oz7oc1JqY7ZwFS76TF77eKHntMbJAjKs8dm4hkBCZj1F9tsEauBuxoVDRbEW
```
and be redirected to your wallet:
```
https://wallet.example.com/#uri=web+tezos:4thVcmfFUC7bLTkuHjMZ9PvdcZgUx9AZ9b6hDa1TbafzNjE8sQ8KMuKEqfjqkVv8pP8fX29d9252TUJXCLdUXSFo1nf2oz7oc1JqY7ZwFS76TF77eKHntMbJAjKs8dm4hkBCZj1F9tsEauBuxoVDRbEW
```
More details are available in [this MDN guide](https://developer.mozilla.org/en-US/docs/Web/API/Navigator/registerProtocolHandler/Web-based_protocol_handlers).

### Decoding the request with tezos-uri
Now that you have the request in your user's address bar, you can decode it and prefill the data contained within. You will need to install the `tezos-uri` package from NPM (it's also available as a bundle from a CDN, see the package [README](https://gitlab.com/smartcontractlabs/tezos-uri/blob/master/README.md)):
```
npm install tezos-uri
```
Now get the needed functions, e.g. with ES import:
```
import { decodeMainnet } from "tezos-uri";
```
and then get the encoded payment request:
```
let hash = window.location.hash; // "#uri=web+tezos:4thVcmfFUC7bLTkuHjMZ9PvdcZgUx9AZ9b6hDa1TbafzNjE8sQ8KMuKEqfjqkVv8pP8fX29d9252TUJXCLdUXSFo1nf2oz7oc1JqY7ZwFS76TF77eKHntMbJAjKs8dm4hkBCZj1F9tsEauBuxoVDRbEW"
let encoded = hash.slice(5); // "web+tezos:4thVcmfFUC7bLTkuHjMZ9PvdcZgUx9AZ9b6hDa1TbafzNjE8sQ8KMuKEqfjqkVv8pP8fX29d9252TUJXCLdUXSFo1nf2oz7oc1JqY7ZwFS76TF77eKHntMbJAjKs8dm4hkBCZj1F9tsEauBuxoVDRbEW"
```
then, use the imported function to decode it:
```
let requests = [];
try {
    requests = decodeMainnet(encoded)
} catch {
    // handle invalid request, tell the user
}
```
If successful, the output will be an array of payment request objects. The decoder will raise an exception if the request is invalid. Detailed specification of the request is available in [the standard](https://gitlab.com/tzip/tzip/blob/master/Proposals/TZIP-0008/TZIP-0008.md)

### Prefilling the transaction details
Now that you have the requests, you can handle the case where there is more than one request (a batch):
```
if (requests.length !== 1) {
    // if the wallet doesnt support batches, tell the user and stop here
}
let request = requests[0];
```
For brevity, we will only describe the simple case with one request here. You can now take the request and prefill the form fields in your UI. The ommitted fields could also be prefilled, where sensible -- e.g. if the user only has one account set up, you could prefill the `source` field.
When the user is done filling in the other fields, you can get the values from the form and reconstruct the operation object (`$operation.alpha.contents`). At this point you can simulate the operation using the [`run_operation`](https://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-helpers-scripts-run-operation) node RPC endpoint. You could also use the `run_operation` endpoint to suggest gas and storage limits, if the user doesn't choose them.

### Injecting the transaction
Now call the [`forge/operations`](https://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-helpers-forge-operations) RPC endpoint as you would normally do (or forge the operation locally), sign the forged bytes and finally inject the transaction using the [`injection/operation`](https://tezos.gitlab.io/mainnet/api/rpc.html#post-injection-operation) endpoint.


## Requesting a Tezos operation
This part is relevant to DApps that want to make it easier for users to interact with them, and wallets that want to offer their users the ability to request payment from other users by simply sending a link.

### Constructing the payment request
The object is based on the objects used by the Tezos JSON-RPC API, with some fields omitted -- these are left to the users or their wallets to fill in. The exact specification can be found [in the standard](https://gitlab.com/tzip/tzip/blob/master/Proposals/TZIP-0008/TZIP-0008.md). We will look at an example using a simple transaction.
```
let request =
{
    "content": {
        "kind": "transaction",
        "amount": "123000",
        "destination": "tz1NAP47ubff4JDLJ94UcwEs66dDDAJGRDwN"
    }
};
```
Since you don't want to request a batch of operations, our array will only contain one:
```
let requests = [request];
```

### Encoding a request with tezos-uri
First, you will need to install the `tezos-uri` package from NPM (it's also available as a bundle from a CDN, see the package [README](https://gitlab.com/smartcontractlabs/tezos-uri/blob/master/README.md)):
```
npm install tezos-uri
```
Now, you have to import the encoder:
```
import { encodeMainnet } from "tezos-uri";
```
Then, you can use it to validate and encode the request:
```
let encoded = "";
try {
    encoded = encodeMainnet(requests);    
} catch {
    // handle invalid input, tell the user and stop here
}

```
The encoder will raise an exception when the request in invalid. If you generated the request from user input, you could tell the user it's invalid and let them change it.
Otherwise the output will be the Tezos URI string.

The encoding is done, you can now let the user copy or click the resulting link.


