type Michelson = MInt | MString | MBytes | MArray | MPrim;

interface MArray extends Array<Michelson> { }

interface MInt {
  int: string;
}

interface MString {
  string: string;
}

interface MBytes {
  bytes: string;
}

interface MPrim {
  prim: string;
  args?: Array<Michelson>;
  annots?: Array<string>;
}

interface Script {
  code: Michelson;
  storage: Michelson;
}

interface Transaction {
  kind: string;
  amount: string;
  destination: string;
  fee?: string;
  gas_limit?: string;
  storage_limit?: string;
  parameters?: Michelson;
}

interface Delegation {
  kind: string;
  delegate: string;
  fee?: string;
  gas_limit?: string;
  storage_limit?: string;
}

interface Origination {
  kind: string;
  balance: string;
  fee?: string;
  gas_limit?: string;
  storage_limit?: string;
  spendable?: boolean;
  delegatable?: boolean;
  delegate?: string;
  script?: Script;
}

interface Request {
  content: Transaction | Delegation | Origination;
  extensions?: object;
}

/**
 * @description Validates and encodes a JSON payment request and returns
 *   a Tezos Mainnet URI. Raises an exception on invalid input.
 * @param {Array<Request>} input JSON payment request
 * @returns {string} Tezos Mainnet URI
 */
export declare function encodeMainnet(input: Array<Request>): string;

/**
 * @description Validates and encodes a JSON payment request and returns
 *   a Tezos Testnet URI. Raises an exception on invalid input.
 * @param {Array<Request>} input JSON payment request
 * @returns {string} Tezos Testnet URI
 */
export declare function encodeTestnet(input: Array<Request>): string;

/**
 * @description Decodes and validates a Tezos URI and returns a resulting
 *   payment request JSON. Raises an exception when the request is invalid,
 *   or when the URI is not a Tezos Mainnet URI.
 * @param {string} uri Tezos Mainnet URI
 * @returns {Array<Request>} payment request JSON
 */
export declare function decodeMainnet(uri: string): Array<Request>;

/**
 * @description Decodes and validates a Tezos URI and returns a resulting
 *   payment request JSON. Raises an exception when the request is invalid,
 *   or when the URI is not a Tezos Testnet URI.
 * @param {string} uri Tezos Testnet URI
 * @returns {Array<Request>} payment request JSON
 */
export declare function decodeTestnet(uri: string): Array<Request>;
