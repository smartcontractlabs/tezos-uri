const path = require("path");

let config = {
  entry: {
    tezosUri: "./src/index.js"
  },
  output: {
    path: path.join(__dirname, "bin/"),
    filename: "[name].js"
  }
};

module.exports = (env, argv) => {
  if (argv.mode === "production") {
    config = {
      entry: {
        tezosUri: "./src/index.js"
      },
      output: {
        path: path.join(__dirname, "bin/"),
        filename: "[name].js",
        libraryTarget: "umd",
        globalObject: "this"
      }
    };
  }
  return config;
};
