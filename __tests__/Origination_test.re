open Jest;
open Expect;
open Origination;

let () = {
  let payload = {|
    {
      "kind": "origination",
      "balance": "123000",
      "delegate": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
    }
  |};

  let corruptedPayloadDelegateContract = {|
    {
      "kind": "origination",
      "balance": "123000",
      "delegate": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg"
    }
  |};

  let corruptedPayloadAddress = {|
    {
      "kind": "origination",
      "balance": "123000",
      "delegate": "tz3Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
    }
  |};

  let corruptedPayloadBnum = {|
    {
      "kind": "origination",
      "balance": "123ff000",
      "delegate": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
    }
  |};

  describe("Origination.fromJson()", () => {
    test("should return origination operation", () =>
      expect(fromJson(Json.parseOrRaise(payload)))
      |> toEqual({
           "kind": "origination",
           "balance": Bignum.make("123000"),
           "fee": None,
           "gas_limit": None,
           "storage_limit": None,
           "delegate":
             Some(Account.make("tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR")),
           "script": None,
         })
    );
    test("should throw with invalid address", () =>
      expect(() =>
        fromJson(Json.parseOrRaise(corruptedPayloadAddress))
      )
      |> toThrowMessage("Invalid checksum")
    );
    test("should throw with invalid num", () =>
      expect(() =>
        fromJson(Json.parseOrRaise(corruptedPayloadBnum))
      )
      |> toThrow
    );
    test("should throw with contract address in delegate field", () =>
      expect(() =>
        fromJson(Json.parseOrRaise(corruptedPayloadDelegateContract))
      )
      |> toThrow
    );
  });
};