open Jest;
open Expect;

let () = {
  let unknownSchemeName = "web+unknown";

  describe("fromString()", () => {
    test("mainnet scheme name should match with SchemeName type", () =>
      expect(Net.fromSchemeName("web+tezos")) |> toBe(Net.Mainnet)
    );
    test("testnet scheme name should match with SchemeName type", () =>
      expect(Net.fromSchemeName("web+tezostestnet")) |> toBe(Net.Testnet)
    );
    test("invalid scheme name should throw", () =>
      expect(() =>
        Net.fromSchemeName(unknownSchemeName)
      )
      |> toThrowMessage(
           "Expected Tezos URI scheme name, got " ++ unknownSchemeName,
         )
    );
  });

  describe("toString()", () => {
    test("SchemeName.Mainnet should return mainnet scheme name", () =>
      expect(Net.toSchemeName(Net.Mainnet)) |> toBe("web+tezos")
    );
    test("SchemeName.Testnet should return testnet scheme name", () =>
      expect(Net.toSchemeName(Net.Testnet)) |> toBe("web+tezostestnet")
    );
  });
};