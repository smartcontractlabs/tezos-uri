open Jest;
open Expect;

let () = {
  let randomString = "sdgkjhseglhrtiguhrughaiufhwogsokjfofj";

  describe("Prefix module fromString() toString()", () => {
    test("should throw with unknown prefix", () =>
      expect(() =>
        Prefix.fromAddressString(randomString)
      )
      |> toThrowMessage("Unknown prefix")
    );
    test("should return Prefix.t from input address", () =>
      expect(
        Prefix.fromAddressString("KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg"),
      )
      |> toBe(Prefix.Contract_hash)
    );
    test("should return string prefix from Prefix.t input", () =>
      expect(Prefix.toString(Prefix.Contract_hash)) |> toBe("KT1")
    );
  });
};