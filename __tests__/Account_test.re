open Jest;
open Expect;

let () = {
  let contractAddress = "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg";

  describe("contract address, account", () =>
    test("should throw on contract address", () =>
      expect(() =>
        Account.validate(contractAddress)
      )
      |> toThrowMessage(
           "Expected address starting with one of tz1, tz2, tz3, but got KT1",
         )
    )
  );
};