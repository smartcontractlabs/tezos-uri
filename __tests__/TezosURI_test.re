open Jest;
open Expect;

let () = {
  let validTestnetUri = "web+tezostestnet:4thVcmfFUC7bLTkuHjMZ9PvdcZgUx9AZ9b6hDa1TbafzNjE8sQ8KMuKEqfjqkXdTWQt5hozC5PSkLAgfRARvpHq3AT36eyFw1aGzGUSPWpjtVe5UK4TryXwTDVMTwuX3CLJJ91eqUJeT4s7RbDzqyYCD";
  let invalidUriMalformed = "web+tezostestnet:4thVcmf:FUC7bLTkuHjMZ9PvdcZgUx9AZ9b6hDa1TbafzNjE8sQ8KMuKEqfjqkXdTWQt5hozC5PSkLAgfRARvpHq3AT36eyFw1aGzGUSPWpjtVe5UK4TryXwTDVMTwuX3CLJJ91eqUJeT4s7RbDzqyYCD";
  let validOrigination = {|
    [
        {
            "content": {
                "kind": "origination",
                "balance": "123000",
                "script": {
                    "code": [
                        {
                            "prim": "parameter",
                            "args": [
                                {
                                    "prim": "unit"
                                }
                            ]
                        },
                        {
                            "prim": "storage",
                            "args": [
                                {
                                    "prim": "unit"
                                }
                            ]
                        },
                        {
                            "prim": "code",
                            "args": [
                                [
                                    {
                                        "prim": "CDR"
                                    },
                                    {
                                        "prim": "NIL",
                                        "args": [
                                            {
                                                "prim": "operation"
                                            }
                                        ]
                                    },
                                    {
                                        "prim": "PAIR"
                                    }
                                ]
                            ]
                        }
                    ],
                    "storage": {
                        "prim": "Unit"
                    }
                },
                "delegate": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
            }
        }
    ]
  |};

  let invalidOriginationDelegate = {|
    [
        {
            "content": {
                "kind": "origination",
                "balance": "123000",
                "delegate": "tzPh8mdwaRp71XvixaExcNKtPvQshe5BwcR"
            }
        }
    ]
  |};

  let extraFieldInMichelsonArgs = {|
    [
        {
            "content": {
                "kind": "transaction",
                "amount": "399000",
                "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg",
                "parameters": {
                   "entrypoint": "test",
                   "value": {
                      "prim": "Left",
                      "args": [
                        {
                          "prim": "Unit",
                          "bar": []
                        }
                      ]
                   }
                }
            }
        }
    ]
  |};

  let missingPrimInParams = {|
    [
        {
            "content": {
                "kind": "transaction",
                "amount": "399000",
                "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg",
                "parameters": {
                    "entrypoint": "test",
                    "value": {
                      "prim": "Left",
                      "args": [
                        {
                            "prim": "Right",
                            "annots": ["foo"],
                            "args": [
                                {
                                    "int": "233"
                                },
                                {
                                    "annots": ["bazqux"]
                                }
                            ]
                        }
                      ]
                    }
                }
            }
        }
    ]
  |};

  let invalidPrimInParams = {|
    [
        {
            "content": {
                "kind": "transaction",
                "amount": "399000",
                "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg",
                "parameters": {
                    "entrypoint": "test",
                    "value": {
                      "prim": "Foo"
                    }
                }
            }
        }
    ]
  |};

  let invalidBytesInParamsUnderscore = {|
    [
        {
            "content": {
                "kind": "transaction",
                "amount": "399000",
                "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg",
                "parameters": {
                    "entrypoint": "test",
                    "value": {
                      "prim": "Left",
                      "args": [
                          {
                              "bytes": "abc_123"
                          }
                      ]
                    }
                }
            }
        }
    ]
  |};

  let invalidBytesInParams = {|
    [
        {
            "content": {
                "kind": "transaction",
                "amount": "399000",
                "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg",
                "parameters": {
                    "entrypoint": "smt",
                    "value": {
                    "prim": "Left",
                    "args": [
                        {
                            "bytes": "[;é"
                        }
                    ]
                    }
                }
            }
        }
    ]
  |};

  let invalidBytesInParamsEmpty = {|
    [
        {
            "content": {
                "kind": "transaction",
                "amount": "399000",
                "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg",
                "parameters": {
                    "entrypoint": "smt",
                    "value": {
                    "prim": "Left",
                    "args": [
                        {
                            "bytes": ""
                        }
                    ]
                    }
                }
            }
        }
    ]
  |};

  let invalidMichelsonArrayInParams = {|
    [
        {
            "content": {
                "kind": "transaction",
                "amount": "399000",
                "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg",
                "parameters": {
                    "entrypoint": "smth",
                    "value": {
                    "prim": "Left",
                    "args": [
                        {
                            "int": "qwer",
                            "string": true,
                            "bytes": "aaa",
                            "prim": "UNIT",
                            "foo": true
                        }
                    ]
                    }
                }
            }
        }
    ]
  |};

  let invalidParamsNumber = {|
    [
        {
            "content": {
                "kind": "transaction",
                "amount": "399000",
                "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg",
                "parameters": 115
            }
        }
    ]
  |};

  let invalidParamsBool = {|
    [
        {
            "content": {
                "kind": "transaction",
                "amount": "399000",
                "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg",
                "parameters": {
                  "entrypoint": "something",
                  "value": true
                }
            }
        }
    ]
  |};

  let invalidArrayArgsParams = {|
    [
        {
            "content": {
                "kind": "transaction",
                "amount": "399000",
                "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg",
                "parameters": {
                    "entrypoint": "smt",
                    "value": {
                    "prim": "Left",
                    "args": [
                        {
                            "prim": "Unit",
                            "args": {
                              "int": "123"
                            }
                        }
                    ]
                    }
                }
            }
        }
    ]
  |};

  let validNestedArrayArgsParams = {|
    [
        {
            "content": {
                "kind": "transaction",
                "amount": "399000",
                "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg",
                "parameters": {
                    "entrypoint": "something",
                    "value": {
                      "prim": "Left",
                      "args": [
                      {
                          "prim": "Unit",
                          "args": [
                            [
                              {"int": "123"}
                            ],
                            {
                              "bytes": "abc123"
                            }
                          ]
                      }
                      ]
                    }
                }
            }
        }
    ]
  |};

  let validParams = {|
    [
        {
            "content": {
                "kind": "transaction",
                "amount": "399000",
                "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg",
                "parameters": {
                    "entrypoint": "smt",
                    "value": {
                    "prim": "Left",
                    "args": [
                      {
                        "int": "46757"
                      },
                      {
                        "string": "mfsalghe"
                      },
                      {
                        "bytes": "QWERa1234zyx"
                      },
                      {
                        "prim": "UNIT",
                        "args": [],
                        "annots": ["foo", "bar"]
                      }
                      ],
                      "annots": []
                    }
                }
            }
        }
    ]
  |};

  let missingKind = {|
    [
        {
            "content": {
                "balance": "123000",
                "delegate": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
            }
        }
    ]
  |};

  let invalidKind = {|
    [
        {
            "content": {
                "kind": "ballot",
                "balance": "123000",
                "delegate": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
            }
        }
    ]
  |};

  let extraFieldInContent = {|
    [
        {
            "content": {
                "kind": "transaction",
                "amount": "123000",
                "destination": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR",
                "foo": "bar"
            }
        }
    ]
  |};

  let emptyExt = {|
    [
        {
            "content": {
                "kind": "delegation",
                "delegate": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
            },
            "extensions": {}
        }
    ]
  |};

  let stringExt = {|
    [
        {
            "content": {
                "kind": "origination",
                "balance": "123000",
                "delegate": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
            },
            "extensions": "123123"
        }
    ]
  |};

  let intExt = {|
    [
        {
            "content": {
                "kind": "origination",
                "balance": "123000",
                "delegate": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
            },
            "extensions": 123123123
        }
    ]
  |};

  let nestedIntExt = {|
    [
        {
            "content": {
                "kind": "origination",
                "balance": "123000",
                "delegate": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
            },
            "extensions": {
                "tip9": 123123123
            }
        }
    ]
  |};

  let nestedStringExt = {|
    [
        {
            "content": {
                "kind": "origination",
                "balance": "123000",
                "delegate": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
            },
            "extensions": {
                "tip9": "vsjandkajsd"
            }
        }
    ]
  |};

  let nestedEmptyExt = {|
    [
        {
            "content": {
                "kind": "origination",
                "balance": "123000",
                "delegate": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
            },
            "extensions": {
                "tip9": {}
            }
        }
    ]
  |};

  let validExt = {|
      [
          {
              "content": {
                  "kind": "origination",
                  "balance": "123000",
                  "delegate": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
              },
              "extensions": {
                  "tip9": { "info": "Some info", "values": 12312312 }
              }
          }
      ]
    |};

  let invalidExt = {|
      [
          {
              "content": {
                  "kind": "origination",
                  "balance": "123000",
                  "delegate": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
              },
              "extensions": {
                  "tip9": { "info": "Some info", "values": 12312312 },
                  "tip10": 123123123
              }
          }
      ]
    |};

  let validExtWithMoreFields = {|
      [
          {
              "content": {
                  "kind": "origination",
                  "balance": "123000",
                  "delegate": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
              },
              "extensions": {
                  "tip9": { "info": "Some info", "values": 12312312 },
                  "tip10": { "info": "Some info", "values": 12312312 },
                  "tip11": { "info": "Some info", "values": 12312312 },
                  "tip12": { "info": "Some info", "values": 12312312 }
              }
          }
      ]
    |};

  let invalidValueInOptionalField = {|
      [
          {
              "content": {
                  "kind": "origination",
                  "balance": "123000",
                  "delegate": "12903819023812903"
              }
          }
      ]
  |};

  // BABYLON TESTS
  let invalidBabylonOriginationObsoleteFields = {|
      [
          {
              "content": {
                  "kind": "origination",
                  "balance": "123000",
                  "spendable": false,
                  "delegatable": false,
                  "delegate": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
              }
          }
      ]
    |};

  let validBabylonTx = {|
    [
      {
        "content": {
          "kind": "transaction",
          "amount": "123000",
          "destination": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR",
          "parameters": {
            "entrypoint": "exampleAction",
            "value": {
              "prim": "Unit",
              "args": []
            }
          }
        }
      }
    ]
  |};

  let invalidBabylonTxParamsExtraField = {|
    [
      {
        "content": {
          "kind": "transaction",
          "amount": "123000",
          "destination": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR",
          "parameters": {
            "entrypoint": "exampleAction",
            "value": {
              "prim": "Unit",
              "args": []
            },
            "foo": "bar"
          }
        }
      }
    ]
  |};

  let validBabylonTxEntrypointEmpty = {|
    [
      {
        "content": {
          "kind": "transaction",
          "amount": "123000",
          "destination": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR",
          "parameters": {
            "entrypoint": "",
            "value": {
              "prim": "Left",
              "args": [
                {
                  "prim": "Unit",
                  "args": []
                }
              ]
            }
          }
        }
      }
    ]
  |};

  let invalidBabylonEntrypointNonstring = {|
    [
      {
        "content": {
          "kind": "transaction",
          "amount": "123000",
          "destination": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR",
          "parameters": {
            "entrypoint": 123,
            "value": {
              "prim": "Left",
              "args": [
                {
                  "prim": "Unit",
                  "args": []
                }
              ]
            }
          }
        }
      }
    ]
  |};

  let validBabylonTxWithExt = {|
    [
    {
        "content": {
            "kind": "transaction",
            "amount": "123000",
            "destination": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR",
            "parameters": {
                "entrypoint": "exampleAction",
                "value": {
                    "prim": "Left",
                    "args": [
                        {
                            "prim": "Unit",
                            "args": []
                        }
                    ]
                }
            }
        },
        "extensions": {
            "tip9": {
                "info": "1 Blockaccino, 1 Scala Chip Frappuccino"
            }
        }
    }
  ]
  |};

  let validBabylonTx1 = {|
    [
      {
        "content": {
          "kind": "transaction",
          "amount": "123000",
          "destination": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
        }
      }
    ]
  |};

  let invalidBabylonParamsValueMissing = {|
    [
      {
        "content": {
          "kind": "transaction",
          "amount": "123000",
          "destination": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR",
          "parameters": {
            "entrypoint": "test"
          }
        }
      }
    ]
  |};

  let invalidBabylonParamsEmpty = {|
    [
      {
        "content": {
          "kind": "transaction",
          "amount": "123000",
          "destination": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR",
          "parameters": {}
        }
      }
    ]
  |};

  let invalidBabylonEntrypointMissing = {|
    [
      {
        "content": {
          "kind": "transaction",
          "amount": "123000",
          "destination": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR",
          "parameters": {
            "value": {
              "prim": "Left",
              "args": [
                {
                  "prim": "Unit",
                  "args": []
                }
              ]
            }
          }
        }
      }
    ]
  |};

  let invalidBabylonTxSchemaOutdated = {|
    [
        {
            "content": {
                "kind": "transaction",
                "amount": "399000",
                "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg",
                "parameters": {
                  "prim": "Left",
                  "args": [
                  {
                      "prim": "Unit",
                      "args": [
                        [
                          {"int": "123"}
                        ],
                        {
                          "bytes": "abc123"
                        }
                      ]
                  }
                  ]
                }
            }
        }
    ]
  |};

  let emptyArray = {|[]|};
  let emptyObjInArray = {|[{}]|};

  let invalidOriginationDelegate =
    Js.Json.parseExn(invalidOriginationDelegate);
  let validOrigination = Js.Json.parseExn(validOrigination);
  let extraFieldInMichelsonArgs = Js.Json.parseExn(extraFieldInMichelsonArgs);
  let missingPrimInParams = Js.Json.parseExn(missingPrimInParams);
  let invalidPrimInParams = Js.Json.parseExn(invalidPrimInParams);
  let invalidBytesInParamsUnderscore =
    Js.Json.parseExn(invalidBytesInParamsUnderscore);
  let invalidBytesInParamsEmpty = Js.Json.parseExn(invalidBytesInParamsEmpty);
  let invalidBytesInParams = Js.Json.parseExn(invalidBytesInParams);
  let invalidMichelsonArrayInParams =
    Js.Json.parseExn(invalidMichelsonArrayInParams);
  let invalidParamsNumber = Js.Json.parseExn(invalidParamsNumber);
  let invalidParamsBool = Js.Json.parseExn(invalidParamsBool);
  let validParams = Js.Json.parseExn(validParams);
  let validNestedArrayArgsParams =
    Js.Json.parseExn(validNestedArrayArgsParams);
  let invalidArrayArgsParams = Js.Json.parseExn(invalidArrayArgsParams);
  let missingKind = Js.Json.parseExn(missingKind);
  let invalidKind = Js.Json.parseExn(invalidKind);
  let extraFieldInContent = Js.Json.parseExn(extraFieldInContent);
  let emptyExt = Js.Json.parseExn(emptyExt);
  let stringExt = Js.Json.parseExn(stringExt);
  let intExt = Js.Json.parseExn(intExt);
  let nestedStringExt = Js.Json.parseExn(nestedStringExt);
  let nestedIntExt = Js.Json.parseExn(nestedIntExt);
  let nestedEmptyExt = Js.Json.parseExn(nestedEmptyExt);
  let validExt = Js.Json.parseExn(validExt);
  let invalidExt = Js.Json.parseExn(invalidExt);
  let validExtWithMoreFields = Js.Json.parseExn(validExtWithMoreFields);
  let invalidValueInOptionalField =
    Js.Json.parseExn(invalidValueInOptionalField);
  let emptyArray = Js.Json.parseExn(emptyArray);
  let emptyObjInArray = Js.Json.parseExn(emptyObjInArray);
  // Babylon tests
  let invalidBabylonOriginationObsoleteFields =
    Js.Json.parseExn(invalidBabylonOriginationObsoleteFields);
  let validBabylonTxWithExt = Js.Json.parseExn(validBabylonTxWithExt);
  let validBabylonTx = Js.Json.parseExn(validBabylonTx);
  let validBabylonTx1 = Js.Json.parseExn(validBabylonTx1);
  let validBabylonTxEntrypointEmpty =
    Js.Json.parseExn(validBabylonTxEntrypointEmpty);
  let invalidBabylonTxParamsExtraField =
    Js.Json.parseExn(invalidBabylonTxParamsExtraField);
  let invalidBabylonParamsValueMissing =
    Js.Json.parseExn(invalidBabylonParamsValueMissing);
  let invalidBabylonParamsEmpty = Js.Json.parseExn(invalidBabylonParamsEmpty);
  let invalidBabylonEntrypointMissing =
    Js.Json.parseExn(invalidBabylonEntrypointMissing);
  let invalidBabylonTxSchemaOutdated =
    Js.Json.parseExn(invalidBabylonTxSchemaOutdated);
  let invalidBabylonEntrypointNonstring =
    Js.Json.parseExn(invalidBabylonEntrypointNonstring);

  describe("decode(encode)", () => {
    test(
      "should be reflexive, i.e. `decode(encode(json))` should equal `json`",
      () =>
      expect(validOrigination |> Encode.mainnet |> Decode.mainnet)
      |> toEqual(validOrigination)
    );
    test(
      "should be reflexive, i.e. `decode(encode(json))` should equal `json`",
      () =>
      expect(validOrigination |> Encode.testnet |> Decode.testnet)
      |> toEqual(validOrigination)
    );
    test(
      "should be reflexive, i.e. `encode(decode(uri))` should equal `uri`", () =>
      expect(validTestnetUri |> Decode.testnet |> Encode.testnet)
      |> toEqual(validTestnetUri)
    );
  });

  describe("decodeMainnet", () => {
    test("should throw when given a valid testnet uri", () =>
      expect(() =>
        validTestnetUri |> Decode.mainnet
      ) |> toThrow
    );
    test("should throw when given an invalid mainnet uri", () =>
      expect(() =>
        invalidUriMalformed |> Decode.mainnet
      ) |> toThrow
    );
  });

  describe("encodeTestnet()", () => {
    test("should throw with invalid delegate address", () =>
      expect(() =>
        invalidOriginationDelegate |> Encode.testnet
      ) |> toThrow
    );
    test("should throw when a required field(`prim`) is missing", () =>
      expect(() =>
        missingPrimInParams |> Encode.testnet
      ) |> toThrow
    );
    test(
      "should throw when a `prim` field does not contain a valid Michelson primitive",
      () =>
      expect(() =>
        invalidPrimInParams |> Encode.testnet
      ) |> toThrow
    );
    test(
      "should throw when Michelson `bytes` field in parameters contains an underscore",
      () =>
      expect(() =>
        invalidBytesInParamsUnderscore |> Encode.testnet
      ) |> toThrow
    );
    test(
      "should throw when Michelson `bytes` field in parameters contains an empty string",
      () =>
      expect(() =>
        invalidBytesInParamsEmpty |> Encode.testnet
      ) |> toThrow
    );
    test(
      "should throw when Michelson `bytes` field in parameters contains non-alphanumeric characters",
      () =>
      expect(() =>
        invalidBytesInParams |> Encode.testnet
      ) |> toThrow
    );
    test(
      "should throw when `args` field in parameters contains an array of invalid Michelson expressions",
      () =>
      expect(() =>
        invalidMichelsonArrayInParams |> Encode.testnet
      ) |> toThrow
    );
    test(
      "should throw when the `parameters` field contains a number instead of a Michelson expression",
      () =>
      expect(() =>
        invalidParamsNumber |> Encode.testnet
      ) |> toThrow
    );
    test(
      "should throw when the `parameters` field contains a boolean instead of a Michelson expression",
      () =>
      expect(() =>
        invalidParamsBool |> Encode.testnet
      ) |> toThrow
    );
    test("should throw when michelson arguments contain an unknown field", () =>
      expect(() =>
        extraFieldInMichelsonArgs |> Encode.testnet
      ) |> toThrow
    );
    test("should throw when `args` field doesn't contain an array", () =>
      expect(() =>
        invalidArrayArgsParams |> Encode.testnet
      ) |> toThrow
    );
    test("should not throw when `args` contains a nested array", () =>
      expect(() =>
        validNestedArrayArgsParams |> Encode.testnet
      )
      |> not_
      |> toThrow
    );
    test("should not throw when the `parameters` object is valid", () =>
      expect(() =>
        validParams |> Encode.testnet
      ) |> not_ |> toThrow
    );
    test("should throw when `kind` is missing", () =>
      expect(() =>
        missingKind |> Encode.testnet
      ) |> toThrow
    );
    test(
      "should throw when `kind` is not transaction, delegation or origination",
      () =>
      expect(() =>
        invalidKind |> Encode.testnet
      ) |> toThrow
    );
    test(
      "should throw when there is extra field that is not optional or required",
      () =>
      expect(() =>
        extraFieldInContent |> Encode.testnet
      ) |> toThrow
    );
    test("should not throw when the `extensions` field is an empty object", () =>
      expect(() =>
        emptyExt |> Encode.testnet
      ) |> not_ |> toThrow
    );
    test("should throw when the `extensions` field contains a string", () =>
      expect(() =>
        stringExt |> Encode.testnet
      ) |> toThrow
    );
    test("should throw when the `extensions` field contains an int", () =>
      expect(() =>
        intExt |> Encode.testnet
      ) |> toThrow
    );
    test(
      "should throw when the nested object in `extensions` contains a string",
      () =>
      expect(() =>
        nestedStringExt |> Encode.testnet
      ) |> toThrow
    );
    test(
      "should throw when the nested object in `extensions` contains an int", () =>
      expect(() =>
        nestedIntExt |> Encode.testnet
      ) |> toThrow
    );
    test(
      "should not throw when the nested object in `extensions` is empty", () =>
      expect(() =>
        nestedEmptyExt |> Encode.testnet
      ) |> not_ |> toThrow
    );
    test(
      "should not throw when the nested object in `extensions` is valid", () =>
      expect(() =>
        validExt |> Encode.testnet
      ) |> not_ |> toThrow
    );
    test(
      "should throw when some of the nested objects in `extensions` are invalid",
      () =>
      expect(() =>
        invalidExt |> Encode.testnet
      ) |> toThrow
    );
    test(
      "should not throw when all of the nested objects in `extensions` are valid",
      () =>
      expect(() =>
        validExtWithMoreFields |> Encode.testnet
      ) |> not_ |> toThrow
    );
    test("should throw when an optional field contains an invalid value", () =>
      expect(() =>
        invalidValueInOptionalField |> Encode.testnet
      ) |> toThrow
    );
    test("should throw with zero requests (an empty array)", () =>
      expect(() =>
        emptyArray |> Encode.testnet
      )
      |> toThrowMessage("Expected one or more requests, got an empty array.")
    );
    test("should throw when there is just an empty object in the array", () =>
      expect(() =>
        emptyObjInArray |> Encode.testnet
      ) |> toThrow
    );
    test("fields spendable and delegatable obsolete in babylon update", () =>
      expect(() =>
        invalidBabylonOriginationObsoleteFields |> Encode.testnet
      )
      |> toThrow
    );
    test("valid babylon transaction, should not throw", () =>
      expect(() =>
        validBabylonTx |> Encode.testnet
      ) |> not_ |> toThrow
    );
    test("valid babylon transaction, should not throw", () =>
      expect(() =>
        validBabylonTx1 |> Encode.testnet
      ) |> not_ |> toThrow
    );
    test(
      "valid babylon transaction with empty string entrypoint, should not throw",
      () =>
      expect(() =>
        validBabylonTxEntrypointEmpty |> Encode.testnet
      )
      |> not_
      |> toThrow
    );
    test("valid babylon transaction with ext should not throw", () =>
      expect(() =>
        validBabylonTxWithExt |> Encode.testnet
      ) |> not_ |> toThrow
    );
    test("invalid babylon transaction, unknown field in parameters object", () =>
      expect(() =>
        invalidBabylonTxParamsExtraField |> Encode.testnet
      )
      |> toThrow
    );
    test("invalid babylon transaction should throw, missing value field", () =>
      expect(() =>
        invalidBabylonParamsValueMissing |> Encode.testnet
      )
      |> toThrow
    );
    test(
      "invalid babylon transaction should throw, empty parameters object", () =>
      expect(() =>
        invalidBabylonParamsEmpty |> Encode.testnet
      ) |> toThrow
    );
    test(
      "invalid babylon transaction should throw, missing entrypoint field", () =>
      expect(() =>
        invalidBabylonEntrypointMissing |> Encode.testnet
      )
      |> toThrow
    );
    test("invalid babylon transaction should throw, old parameters schema", () =>
      expect(() =>
        invalidBabylonTxSchemaOutdated |> Encode.testnet
      ) |> toThrow
    );
    test(
      "invalid babylon transaction should throw, non-string value in entrypoint",
      () =>
      expect(() =>
        invalidBabylonEntrypointNonstring |> Encode.testnet
      )
      |> toThrow
    );
  });
};