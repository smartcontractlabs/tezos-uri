open Jest;
open Expect;
open Delegation;

let () = {
  let payload = {|
    {
      "kind": "delegation",
      "fee": "1500",
      "gas_limit": "10100",
      "storage_limit": "0",
      "delegate": "tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3"
    }
  |};

  let corruptedPayloadAddress = {|
    {
      "kind": "delegation",
      "fee": "1500",
      "gas_limit": "10100",
      "storage_limit": "0",
      "delegate": "tz24X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3"
    }
  |};

  let corruptedPayloadBnum = {|
    {
      "kind": "delegation",
      "fee": "150sdd0",
      "gas_limit": "10100",
      "storage_limit": "0",
      "delegate": "tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3"
    }
  |};

  describe("Delegation.fromJson()", () => {
    test("should return delegation operation", () =>
      expect(fromJson(Json.parseOrRaise(payload)))
      |> toEqual({
           "kind": "delegation",
           "delegate": Account.make("tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3"),
           "fee": Some(Bignum.make("1500")),
           "gas_limit": Some(Bignum.make("10100")),
           "storage_limit": Some(Bignum.make("0")),
         })
    );
    test("should throw with invalid address", () =>
      expect(() =>
        fromJson(Json.parseOrRaise(corruptedPayloadAddress))
      )
      |> toThrowMessage(
           "Expected address of 36 characters, but got 37 in tz24X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3",
         )
    );
    test("should throw with invalid num", () =>
      expect(() =>
        fromJson(Json.parseOrRaise(corruptedPayloadBnum))
      )
      |> toThrow
    );
  });
};