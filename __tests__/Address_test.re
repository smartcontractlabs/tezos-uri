open Jest;
open Expect;
open Address;

let () = {
  describe("check address length", () => {
    test("should throw when address is shorter then expected length (36)", () => {
      let address = "abc";
      let addressLength = string_of_int @@ Js.String.length(address);
      expect(() =>
        checkLength(address)
      )
      |> toThrowMessage(
           {j|Expected address of $expectedLength characters, but got $addressLength in $address|j},
         );
    });
    test("should throw when address is longer then expected length (36)", () => {
      let address = "sdgkjhseglhrtiguhrughaiufhwogsokjfofj";
      let addressLength = string_of_int @@ Js.String.length(address);
      expect(() =>
        checkLength(address)
      )
      |> toThrowMessage(
           {j|Expected address of $expectedLength characters, but got $addressLength in $address|j},
         );
    });
  });
};