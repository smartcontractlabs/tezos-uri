open Jest;
open Expect;
open Bs58check;

let () = {
  let encoded = "5Kd3NBUAdUnhyzenEwVLy9pBKxSwXvE9FMPyR4UKZvpe6E3AgLr";
  let decoded =
    Js.Typed_array.Uint8Array.make([|
      128,
      237,
      219,
      220,
      17,
      104,
      241,
      218,
      234,
      219,
      211,
      228,
      76,
      30,
      63,
      143,
      90,
      40,
      76,
      32,
      41,
      247,
      138,
      210,
      106,
      249,
      133,
      131,
      164,
      153,
      222,
      91,
      25,
    |]);

  let corruptedAddress = "tz1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg";

  describe("decode()", () => {
    test("should return buffer", () =>
      expect(decode(encoded)) |> toEqual(Buffer.fromArray(decoded))
    );
    test("should throw with corrupted address", () =>
      expect(() =>
        decode(corruptedAddress)
      )
      |> toThrowMessage("Invalid checksum")
    );
  });

  describe("encode()", () =>
    test("should return string", () =>
      expect(encode(Buffer.fromArray(decoded))) |> toBe(encoded)
    )
  );
};