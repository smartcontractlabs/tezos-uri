open Jest;
open Expect;
open Transaction;

let () = {
  let txPayload = {|
    {
      "kind": "transaction",
      "amount": "399000",
      "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg",
      "parameters": {
        "entrypoint": "something",
        "value": {
          "prim": "Left",
          "args": [
            {
              "prim": "Unit",
              "args": []
            }
          ]
        }
      }
    }
  |};

  let corruptedPayloadAddress = {|
    {
      "kind": "transaction",
      "amount": "399000",
      "destination": "tz3W6dVQvpeAsnjfwKvoPBJViRKaa7P9HJ99"
    }
  |};

  let corruptedPayloadBnum = {|
    {
      "kind": "transaction",
      "amount": "399df000",
      "destination": "KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg"
    }
  |};

  describe("Transaction.fromJson()", () => {
    test("should return transaction operation", () =>
      expect(fromJson(Json.parseOrRaise(txPayload)))
      |> toEqual({
           "kind": "transaction",
           "amount": Bignum.make("399000"),
           "destination":
             Address.make("KT1MFdSrM6LqGXaXAWuuXX4tWe8H3n5Xekyg"),
           "fee": None,
           "gas_limit": None,
           "storage_limit": None,
           "parameters":
             Some({
               "entrypoint": "something",
               "value":
                 Michelson.MPrim({
                   "prim": "Left",
                   "args":
                     Some([|
                       Michelson.MPrim({
                         "prim": "Unit",
                         "args": Some([||]),
                         "annots": None,
                       }),
                     |]),
                   "annots": None,
                 }),
             }),
         })
    );
    test("should throw with invalid address", () =>
      expect(() =>
        fromJson(Json.parseOrRaise(corruptedPayloadAddress))
      )
      |> toThrow
    );
    test("should throw with invalid amount", () =>
      expect(() =>
        fromJson(Json.parseOrRaise(corruptedPayloadBnum))
      )
      |> toThrow
    );
  });
};