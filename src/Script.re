/** Tezos contract script, similar to $scripted.contracts in the JSON-RPC API. */
type t = {
  .
  "code": Michelson.t,
  "storage": Michelson.t,
};

open Json.Decode;

let decode = json => {
  "code": json |> field("code", Michelson.decode),
  "storage": json |> field("storage", Michelson.decode),
};

/** Decodes a JSON object into a Michelson script object. Raises an exception when required fields are missing or unknown fields are present.

It will allow any object that satisfies the JSON-RPC spec -- in particular, it will allow any primitive to have any `args` or `annots`. */
let fromJson: Js.Json.t => t = Utils.strictDecoder(decode);