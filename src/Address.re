/** A Tezos address, represented by a string. It can be either an account (implicit contract) or a contract. Similar to $contract_id in the JSON-RPC API. */
type t = string;
let expectedLength = 36;

let checkLength = (addr: string): unit =>
  if (String.length(addr) !== expectedLength) {
    raise @@
    Json.Decode.DecodeError(
      "Expected address of "
      ++ string_of_int(expectedLength)
      ++ " characters, but got "
      ++ string_of_int(Js.String.length(addr))
      ++ " in "
      ++ addr,
    );
  };

let validate = (addr: string): t => {
  let _ = Prefix.fromAddressString(addr);
  checkLength(addr);
  let _ = Bs58check.decode(addr);
  addr;
};

/** Checks that the given string is a valid representation of a Tezos address (public key hash or contract hash) and returns an `Address.t`. Raises an exception otherwise. */
let make = validate;

/** Checks that the JSON value is a valid string representation of a Tezos address (public key hash or contract hash) and returns an `Address.t`. Raises an exception otherwise. */
let decode = (json: Js.Json.t): t => {
  json |> Json.Decode.string |> validate;
};