open Json.Decode;

/** Tezos transaction parameters. */
type t = {
  .
  "entrypoint": string,
  "value": Michelson.t,
};

let decode = json => {
  "entrypoint": json |> field("entrypoint", string),
  "value": json |> field("value", Michelson.decode),
};

/** Decodes a JSON object into Tezos transaction parameters. Raises an exception when required fields are missing or unknown fields are present. */
let fromJson: Js.Json.t => t = Utils.strictDecoder(decode);