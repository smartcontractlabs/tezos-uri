open Json.Decode;

/** A Tezos operation. It is similar to $operation.alpha.contents in the JSON-RPC API. */
type t =
  | Delegation(Delegation.t)
  | Origination(Origination.t)
  | Transaction(Transaction.t);

/** Decodes a JSON object into a Tezos operation -- depending on the kind field, returns either a transaction, origination or delegation. Raises an exception when required fields are missing or unknown fields are present. */
let fromJson = (json: Js.Json.t): t => {
  let kind = json |> field("kind", string);
  switch (kind) {
  | "transaction" => Transaction(Transaction.fromJson(json))
  | "origination" => Origination(Origination.fromJson(json))
  | "delegation" => Delegation(Delegation.fromJson(json))
  | _ =>
    raise @@
    Json.Decode.DecodeError(
      "Expected kind of delegation, origination or transaction, but got "
      ++ kind,
    )
  };
};