/** Positive arbitrary length integer, represented by a string of decimal digits. Similar to $positive_bignum in the JSON-RPC API. */
type t = string;

let validate = (bnum: string): t => {
  // match non-digit characters
  switch (Js.String.match([%re "/\\D+/g"], bnum)) {
  | Some(matches) =>
    raise @@
    Json.Decode.DecodeError(
      "Expected Tezos amount, got invalid character(s) "
      ++ Js.Array.toString(matches)
      ++ " in "
      ++ bnum,
    )
  | None => bnum
  };
};

/** Checks that the string is a valid representation of a positive bignum and returns a `Bignum.t`. Raises an exception when non-decimal characters are present. */
let make = validate;

/** Checks that the JSON value is a valid string representation of a positive bignum and returns a `Bignum.t`. Raises an exception when non-decimal characters are present. */
let decode = (json: Js.Json.t): t => {
  json |> Json.Decode.string |> validate;
};