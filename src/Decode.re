/** Decodes and validates a Tezos URI and returns the resulting payment request JSON. Raises an exception when the request is invalid, or when the URI does not match the expected network. */
let withNet = (expected: Net.t, uri: string): Js.Json.t => {
  let chunks = Js.String.split(":", uri);
  if (Js.Array.length(chunks) !== 2) {
    raise @@ Json.Decode.DecodeError("Malformed tezos URI: " ++ uri);
  };

  // check that the URI is for the expected network
  if (Net.fromSchemeName(chunks[0]) !== expected) {
    raise @@
    Json.Decode.DecodeError(
      "Expected " ++ Net.toSchemeName(expected) ++ " but got " ++ chunks[0],
    );
  };

  // decode the URI payload
  let json =
    chunks[1] |> Bs58check.decode |> Node_buffer.toString |> Json.parseOrRaise;

  // decode the request
  json |> Json.Decode.array(Request.fromJson) |> ignore;

  // return the resulting JSON
  json;
};

/** Decodes and validates a Tezos URI and returns the resulting payment request JSON. Raises an exception when the request is invalid, or when the URI is not a Tezos Mainnet URI. */
let mainnet = (uri: string): Js.Json.t => withNet(Net.Mainnet, uri);

/** Decodes and validates a Tezos URI and returns the resulting payment request JSON. Raises an exception when the request is invalid, or when the URI is not a Tezos Testnet URI. */
let testnet = (uri: string): Js.Json.t => withNet(Net.Testnet, uri);