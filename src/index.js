import * as encode from "./Encode.bs";
import * as decode from "./Decode.bs";

export const encodeMainnet = encode.mainnet;
export const encodeTestnet = encode.testnet;
export const decodeMainnet = decode.mainnet;
export const decodeTestnet = decode.testnet;
