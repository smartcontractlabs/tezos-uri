[@bs.get] external length: Node_buffer.t => int = "length";
[@bs.send] external equals: (Node_buffer.t, Node_buffer.t) => bool = "equals";
[@bs.val] [@bs.scope "Buffer"]
external fromArray: Js.Typed_array.Uint8Array.t => Node_buffer.t = "from";
[@bs.send]
external slice: (Node_buffer.t, int, int) => Node_buffer.t = "slice";

[@bs.send]
external toStringWithEncoding:
  (
    Node_buffer.t,
    [@bs.string] [
      | `ascii
      | `utf8
      | `utf16le
      | `usc2
      | `base64
      | `latin1
      | `binary
      | `hex
    ]
  ) =>
  string =
  "toString";