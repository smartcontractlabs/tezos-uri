/** Decodes a b58check string into a buffer. Raises an exception when the checksum is invalid and when the string contains non-base58 characters.

Binding to bs58check from npm. */
[@bs.module "bs58check"]
external decode: string => Node_buffer.t = "decode";

/** Encodes a buffer into b58check string.

Binding to bs58check from npm. */
[@bs.module "bs58check"]
external encode: Node_buffer.t => string = "encode";