/** Michelson expression, similar to $micheline.michelson_v1.expression in the JSON-RPC API. */
type t =
  | MInt({. "int": Bignum.t})
  | MString({. "string": string})
  | MBytes({. "bytes": Bytes.t})
  | MArray(array(t))
  | MPrim(
      {
        .
        "prim": Prim.t,
        "args": option(array(t)),
        "annots": option(array(string)),
      },
    );

open Json.Decode;
open Utils; // optionalField, strictDecoder

/** Decodes a JSON object into a Michelson int object. Raises an exception when the `bytes` field is missing or unknown fields are present. */
let mIntDecode = (j: Js.Json.t): t => {
  let decoded =
    try (
      j
      |> strictDecoder(json => {"int": json |> field("int", Bignum.decode)})
    ) {
    | Json.Decode.DecodeError(err) =>
      raise @@ Json.Decode.DecodeError(err ++ " in Michelson int object")
    };
  MInt(decoded);
};

/** Decodes a JSON object into a Michelson string object. Raises an exception when the `bytes` field is missing or unknown fields are present. */
let mStringDecode = (j: Js.Json.t): t => {
  let decoded =
    try (
      j |> strictDecoder(json => {"string": json |> field("string", string)})
    ) {
    | Json.Decode.DecodeError(err) =>
      raise @@ Json.Decode.DecodeError(err ++ " in Michelson string object")
    };
  MString(decoded);
};

/** Decodes a JSON object into a Michelson bytes object. Raises an exception when the `bytes` field is missing or unknown fields are present.

It will allow any object that satisfies the JSON-RPC spec -- in particular, it will allow a byte string to contain any alphanumeric characters, excluding underscore. */
let mBytesDecode = (j: Js.Json.t): t => {
  let decoded =
    try (
      j
      |> strictDecoder(json =>
           {"bytes": json |> field("bytes", Bytes.decode)}
         )
    ) {
    | Json.Decode.DecodeError(err) =>
      raise @@ Json.Decode.DecodeError(err ++ " in Michelson bytes object")
    };
  MBytes(decoded);
};

/** Decodes a JSON object into a Michelson primitive object. Raises an exception when the `prim` field is missing or unknown fields are present.

It will allow any object that satisfies the JSON-RPC spec -- in particular, it will allow any primitive to have any `args` or `annots`. */
let rec mPrimDecode = (j: Js.Json.t): t => {
  let decoded =
    try (
      j
      |> strictDecoder(json =>
           {
             "prim": json |> field("prim", Prim.decode),
             "args": json |> optionalField("args", array(decode)),
             "annots": json |> optionalField("annots", array(string)),
           }
         )
    ) {
    | Json.Decode.DecodeError(err) =>
      raise @@ Json.Decode.DecodeError(err ++ " in Michelson prim object")
    };
  MPrim(decoded);
}
/** Decodes a JSON array into an array of Michelson expressions. */
and mArrayDecode = (json: Js.Json.t): t => {
  MArray(
    {
      json |> array(decode);
    },
  );
}
/** Decodes a JSON object into a Michelson expression. Raises an exception when required fields are missing or unknown fields are present.

It will allow any object that satisfies the JSON-RPC spec -- in particular, it will allow any primitive to have any `args` or `annots`. */
and decode = (json: Js.Json.t): t => {
  json
  |> Utils.oneOf([
       mIntDecode,
       mStringDecode,
       mBytesDecode,
       mPrimDecode,
       mArrayDecode,
     ]);
};