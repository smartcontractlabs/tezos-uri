/** A Tezos address prefix. */
type t =
  | Ed25519_public_key_hash
  | Secp256k1_public_key_hash
  | P256_public_key_hash
  | Contract_hash;

/** When the string starts with a valid address prefix, returns the matching `Prefix.t`. Otherwise, raises an exception */
let fromAddressString = (s: string): t => {
  switch (s) {
  | x when Js.String.startsWith("tz1", x) => Ed25519_public_key_hash
  | x when Js.String.startsWith("tz2", x) => Secp256k1_public_key_hash
  | x when Js.String.startsWith("tz3", x) => P256_public_key_hash
  | x when Js.String.startsWith("KT1", x) => Contract_hash
  | unknown =>
    raise @@ Json.Decode.DecodeError("Unknown prefix in " ++ unknown)
  };
};

/** Takes a `Prefix.t` and returns its matching string representation. */
let toString = (prefix: t): string => {
  switch (prefix) {
  | Ed25519_public_key_hash => "tz1"
  | Secp256k1_public_key_hash => "tz2"
  | P256_public_key_hash => "tz3"
  | Contract_hash => "KT1"
  };
};