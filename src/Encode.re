/** Validates and encodes a JSON payment request and returns a Tezos URI. Raises an exception on invalid input. */
let withNet = (net: Net.t, input: Js.Json.t): string => {
  // validate the input by decoding it
  input
  |> Json.Decode.array(Request.fromJson)
  |> Js.Array.length
  |> (
    l =>
      switch (l) {
      | 0 =>
        raise @@
        Json.Decode.DecodeError(
          "Expected one or more requests, got an empty array.",
        )
      | _ => ()
      }
  );

  // encode the payload JSON
  let payload =
    input |> Js.Json.stringify |> Node_buffer.fromString |> Bs58check.encode;

  // return the resulting URI
  Net.toSchemeName(net) ++ ":" ++ payload;
};

/** Validates and encodes a JSON payment request and returns a Tezos Mainnet URI. Raises an exception on invalid input. */
let mainnet = (input: Js.Json.t): string => withNet(Net.Mainnet, input);

/** Validates and encodes a JSON payment request and returns a Tezos Testnet URI. Raises an exception on invalid input. */
let testnet = (input: Js.Json.t): string => withNet(Net.Testnet, input);