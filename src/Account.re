/** A Tezos account (implicit contract), represented by a string. It can only be an account (implicit contract), not a regular contract (starting with KT1). Similar to $Signature.Public_key_hash in the JSON-RPC API. */
type t = string;

let validate = (acc: string): t => {
  acc
  |> Prefix.fromAddressString
  |> (
    prefix =>
      switch (prefix) {
      | Prefix.Ed25519_public_key_hash
      | Prefix.Secp256k1_public_key_hash
      | Prefix.P256_public_key_hash => ()
      | Prefix.Contract_hash =>
        raise @@
        Json.Decode.DecodeError(
          "Expected address starting with one of tz1, tz2, tz3, but got KT1",
        )
      }
  );

  Address.checkLength(acc);
  let _ = Bs58check.decode(acc);
  acc;
};

/** Checks that the string is a valid representation of a Tezos account (public key hash) and returns an `Account.t`. Raises an exception otherwise. */
let make = validate;

/** Checks that the JSON value is a valid string representation of a Tezos account (public key hash) and returns an `Account.t`. Raises an exception otherwise. */
let decode = (json: Js.Json.t): t => {
  json |> Json.Decode.string |> validate;
};