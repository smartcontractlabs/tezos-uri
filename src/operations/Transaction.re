open Json.Decode;

/** Tezos transaction operation, similar to the transaction variant of $operation.alpha.contents in the JSON-RPC API. */
type t = {
  .
  "kind": string,
  "amount": Bignum.t,
  "destination": Address.t,
  "fee": option(Bignum.t),
  "gas_limit": option(Bignum.t),
  "storage_limit": option(Bignum.t),
  "parameters": option(Parameters.t),
};

let decode = json => {
  "kind": json |> field("kind", string),
  "amount": json |> field("amount", Bignum.decode),
  "destination": json |> field("destination", Address.decode),
  "fee": json |> Utils.optionalField("fee", Bignum.decode),
  "gas_limit": json |> Utils.optionalField("gas_limit", Bignum.decode),
  "storage_limit":
    json |> Utils.optionalField("storage_limit", Bignum.decode),
  "parameters":
    json |> Utils.optionalField("parameters", Parameters.fromJson),
};

/** Decodes a JSON object into a Tezos transaction operation. Raises an exception when required fields are missing or unknown fields are present. */
let fromJson: Js.Json.t => t = Utils.strictDecoder(decode);