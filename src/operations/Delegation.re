open Json.Decode;

/** Tezos delegation operation, similar to the delegation variant of $operation.alpha.contents in the JSON-RPC API. */
type t = {
  .
  "kind": string,
  "delegate": Account.t,
  "fee": option(Bignum.t),
  "gas_limit": option(Bignum.t),
  "storage_limit": option(Bignum.t),
};

let decode = json => {
  "kind": json |> field("kind", string),
  "delegate": json |> field("delegate", Account.decode),
  "fee": json |> Utils.optionalField("fee", Bignum.decode),
  "gas_limit": json |> Utils.optionalField("gas_limit", Bignum.decode),
  "storage_limit":
    json |> Utils.optionalField("storage_limit", Bignum.decode),
};

/** Decodes a JSON object into a Tezos delegation operation. Raises an exception when required fields are missing or unknown fields are present. */
let fromJson: Js.Json.t => t = Utils.strictDecoder(decode);