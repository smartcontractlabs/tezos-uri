open Json.Decode;

/** Tezos origination operation, similar to the origination variant of $operation.alpha.contents in the JSON-RPC API. */
type t = {
  .
  "kind": string,
  "balance": Bignum.t,
  "fee": option(Bignum.t),
  "gas_limit": option(Bignum.t),
  "storage_limit": option(Bignum.t),
  "delegate": option(Account.t),
  "script": option(Script.t),
};

let decode = json => {
  "kind": json |> field("kind", string),
  "balance": json |> field("balance", Bignum.decode),
  "fee": json |> Utils.optionalField("fee", Bignum.decode),
  "gas_limit": json |> Utils.optionalField("gas_limit", Bignum.decode),
  "storage_limit":
    json |> Utils.optionalField("storage_limit", Bignum.decode),
  "delegate": json |> Utils.optionalField("delegate", Account.decode),
  "script": json |> Utils.optionalField("script", Script.fromJson),
};

/** Decodes a JSON object into a Tezos origination operation. Raises an exception when required fields are missing or unknown fields are present. */
let fromJson: Js.Json.t => t = Utils.strictDecoder(decode);