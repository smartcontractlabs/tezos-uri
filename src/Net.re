type t =
  | Mainnet
  | Testnet;

/** Takes a `Net.t` and returns a matching URI scheme name string of a Tezos URI. */
let toSchemeName = (net: t): string => {
  switch (net) {
  | Mainnet => "web+tezos"
  | Testnet => "web+tezostestnet"
  };
};

/** If the string is a valid Tezos URI scheme name, returns a matching `Net.t`.
Raises an exception otherwise. */
let fromSchemeName = (s: string): t => {
  switch (s) {
  | "web+tezos" => Mainnet
  | "web+tezostestnet" => Testnet
  | unknown =>
    raise @@
    Json.Decode.DecodeError("Expected Tezos URI scheme name, got " ++ unknown)
  };
};