/** Provides a strict JSON object decoder.

`Json.Decode.decoder` skips unknown fields. Given such decoder, strictDecoder returns a decoder that instead raises an exception on unknown fields. */
let strictDecoder =
    (decoder: Json.Decode.decoder(Js.t('a)))
    : Json.Decode.decoder(Js.t('a)) => {
  json => {
    let decoded = decoder(json);
    json
    |> Js.Json.decodeObject
    |> Belt.Option.getExn
    |> Js.Dict.keys
    |> Js.Array.forEach(field =>
         decoded  // the output from the original decoder doesn't contain the unknown fields, so we check against it
         |> Js.Obj.keys
         |> Js.Array.includes(field)
         |> (
           found =>
             switch (found) {
             | false =>
               raise @@
               Json.Decode.DecodeError("Unknown field: '" ++ field ++ "'")
             | true => ()
             }
         )
       );
    decoded;
  };
};

/** Provides a strict JSON decoder for optional object fields

Almost like `Json.Decode.optional`, but passes through all exceptions other than the missing field exception. When the field is present but its value invalid, an exception will be raised. */
let optionalField =
    (field: string, decoder: Json.Decode.decoder('a), json: Js.Json.t)
    : option('a) =>
  try (Some(json |> Json.Decode.field(field, decoder))) {
  | Json.Decode.DecodeError(str)
      when str |> Js.String.startsWith("Expected field '" ++ field ++ "'") =>
    None
  | Json.Decode.DecodeError(err) => raise @@ Json.Decode.DecodeError(err)
  };

/** When the option contains `Some(x)` returns `x`, otherwise raises a `Json.Decode.DecodeError` with the given error message. */
let someOrDecodeError = (err: string, opt: option('a)): 'a => {
  switch (opt) {
  | Some(x) => x
  | None => raise @@ Json.Decode.DecodeError(err)
  };
};

/** `Json.Decode.oneOf` with nicer formatting of errors.

Tries each `decoder` in order, returning the result of the first that succeeds.

**Returns** an `'a` if one of the decoders succeed.

Raises: [DecodeError] if unsuccessful */
let oneOf = (decoders: list(Json.Decode.decoder('a)), json: Js.Json.t): 'a => {
  let rec inner = (decoders, errors) =>
    switch (decoders) {
    | [] =>
      let revErrors = List.rev(errors);
      raise @@
      Json.Decode.DecodeError(
        {j|All decoders given to oneOf failed. Here are all the errors: $revErrors. And the JSON being decoded: |j}
        ++ Js.Json.stringifyWithSpace(json, 2),
      );
    | [decode, ...rest] =>
      try (decode(json)) {
      | Json.Decode.DecodeError(e) => inner(rest, ["\n" ++ e, ...errors])
      }
    };
  inner(decoders, []);
};