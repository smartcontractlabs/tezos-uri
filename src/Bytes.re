/** Bytes, represented by a string of alphanumeric charaters, excluding underscore. This is inline with the JSON-RPC spec, even though the field seems to only ever contain hex encoded bytes -- [a-fA-F0-9].

Used in e.g. `$micheline.michelson_v1.expression` in the JSON-RPC API.  */
type t = string;

let validate = (str: string): t => {
  // match non-alphanumeric characters, (including underscore)
  switch (Js.String.match([%re "/^[a-zA-Z0-9]+$/"], str)) {
  | None =>
    switch (Js.String.match([%re "/[^a-zA-Z0-9]+/g"], str)) {
    | None =>
      raise @@
      Json.Decode.DecodeError(
        "Expected Michelson byte string, got an empty string",
      )
    | Some(matches) =>
      raise @@
      Json.Decode.DecodeError(
        "Expected Michelson byte string, got invalid character(s) "
        ++ Js.Array.toString(matches)
        ++ " in "
        ++ str,
      )
    }
  | Some(_match) => str
  };
};

/** Checks that the string is a valid representation of bytes and returns a `Bytes.t`. Raises an exception when non-alphanumeric characters, including underscore, are present. */
let make = validate;

/** Checks that the JSON value is a valid string representation of bytes and returns a `Bytes.t`. Raises an exception when non-alphanumeric characters, including underscore, are present. */
let decode = (json: Js.Json.t): t => {
  json |> Json.Decode.string |> validate;
};