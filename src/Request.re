open Json.Decode;

/** A Tezos request. This has no counterpart in the JSON-RPC API -- it contains a Tezos operation (similar to [$operation.alpha.contents]) and an extension field to allow wallets to add more functionality, such as a note for each payment request. */
type t = {
  .
  "content": Operation.t,
  "extensions": option(Js.Json.t),
};

/** Checks that the JSON is valid extensions object -- it can only contain the individual extension objects. */
let extensionsDecode = (json: Js.Json.t) => {
  json
  |> Js.Json.decodeObject
  |> Utils.someOrDecodeError(
       "Expected extensions object, got: "
       ++ Js.Json.stringifyWithSpace(json, 2),
     )
  |> Js.Dict.values
  |> Js.Array.forEach(v =>
       v
       |> Js.Json.decodeObject
       |> Utils.someOrDecodeError(
            "Expected one or more extension objects in the `extensions` object, got: "
            ++ Js.Json.stringifyWithSpace(json, 2),
          )
       |> ignore
     );

  json;
};

let decode = json => {
  "content": json |> field("content", Operation.fromJson),
  "extensions": json |> Utils.optionalField("extensions", extensionsDecode),
};

/** Decodes a JSON object into a Tezos request. Raises an exception when required fields are missing or unknown fields are present. */
let fromJson: Js.Json.t => t = Utils.strictDecoder(decode);